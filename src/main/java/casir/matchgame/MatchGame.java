package casir.matchgame;

import java.lang.Exception;
import java.util.Scanner;
import java.util.Random;

public class MatchGame {

    public static void main(String[] args){
        int remaining = 10;
        while (remaining > 0) {
            Scanner reader = new Scanner(System.in);
            System.out.println("Il reste " + remaining + "  allumettes.");
            System.out.println("Enter a number between 1 and 3: ");
            int n = reader.nextInt();
            
            remaining = retirerAllumette(remaining,n);
            if (remaining == 1){
                System.out.println("You Win");
                break;
            }

            remaining = retirerAllumetteBot(remaining);
            if (remaining == 1){
                System.out.println("You Loose");
                break;
            }
        }
    }
    
    public static int retirerAllumette(int r, int n) {
    	r -= n;
    	System.out.println("Vous enlevez "+ n +": il reste "+ r +"allumettes.");
    	return r;
    }
    
    public static int retirerAllumetteBot(int r) {
    	int n = new Random().nextInt(2) + 1;
        r -= n;
    	System.out.println("Bot enleve "+ n +": il reste "+ r +"allumettes.");
    	return r;
    }
    
   
    public static int add(int a, int b) throws Exception {
        int c = a + b;
        if (c == 0){
            throw new Exception("c is zeroooo !");
        }
        return c;
    }
}